import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

class RunSpecification extends Specification{

    @Shared
    Run sharedRun;

    def setupSpec(){
        sharedRun = new Run()
    }


    @Unroll
    def "test that calculateCaloriesBurned must pass the test with values below"(){

        given:

        sharedRun.setDistance(a)
        sharedRun.setStepsPerKm(b)
        sharedRun.setStepsPerCalorie(c)

        expect:
        sharedRun.calculateCaloriesBurned() == d

        where:
        a| b| c || d
        10 | 500 | 4 || 1250
        20 | 1000 | 8 || 2500
        4  | 2 | 1 || 8
        5  | 1 | 5 || 1
        0  | 1 | 1 || 0
        0  | 0 | 1 || 0
    }



    def "test that calculateCaloriesBurned pass the "(){
        given:

        sharedRun.setDistance(10)
        sharedRun.setStepsPerKm(500)
        sharedRun.setStepsPerCalorie(4)

        expect:
        sharedRun.calculateCaloriesBurned() == 1250


    }
}
