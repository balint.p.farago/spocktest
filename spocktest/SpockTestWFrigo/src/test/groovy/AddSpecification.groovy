import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

class AddSpecification extends Specification {

    @Shared
    Add sharedAdd

    def setupSpec() {
        sharedAdd = new Add()
    }

//    def "test that add adds two numbers correctly"() {
//        when:
//        println "valami"
//
//        then:
//        sharedAdd.addNumbers(1, 2) == 3
//    }

    @Unroll
    def "tests that add adds #a and #b with result #c"() {
        expect:
        sharedAdd.divide(a, b) == c

        where:
         a | b || c
        10 | 2 || 5
        1 | 2 || 5
        -1 | 2 || 0
        0 | 1 || 0
    }

    def "test that divide throws exception when dividing by zero"() {
        when:
        sharedAdd.divide(1, 0) == 3

        then:
        thrown(ArithmeticException)

    }
}
