import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

class BikeSpecification extends Specification {

    @Shared
    Bike sharedBike;

    def setupSpec() {
        sharedBike = new Bike()
    }

    @Unroll
    def "test that calculateCaloriesBurned must pass the test with values below"() {
        given:
        sharedBike.setAvaragePower(b)
        sharedBike.setDuration(a)

        expect:
        sharedBike.calculateCaloriesBurned() == c

        where:
        a | b || c
        1 | 1 || 3600
        600 | 0 || 0
        2 | 2 || 14400
    }
}
