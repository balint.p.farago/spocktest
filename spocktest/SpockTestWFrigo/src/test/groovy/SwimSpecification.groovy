import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

class SwimSpecification extends Specification{

    @Shared
    Swim sharedSwim;

    def setupSpec(){
        sharedSwim = new Swim()
    }

    @Unroll
    def "test that calculateCaloriesBurned must pass the test with values below"(){
        given:
        sharedSwim.setDistance(a)
        sharedSwim.setPoolLength(b)
        sharedSwim.setCalPer100Stroke(c)
        sharedSwim.setStrokePerLength(d)

        expect:
        sharedSwim.calculateCaloriesBurned() == e

        where:
        a | b | c | d || e
        5 | 2 | 20 | 200 || 100
        10 | 4 | 40 | 200 || 200
        1  | 4 | 3 | 100 || 0
        1  | 4 | 4 | 100 || 1
        1  | 1 | 0 | 10000 || 0
        1  | 1 | 0 | 10000 || 0
        1  | 1 | 1 | 10000 || 100



    }

}
