public class Run extends Workout{

    //defines how many steps it takes to burn 1 calorie
    private int stepsPerCalorie;
    //defines how many steps the user took to cover 1 km during the workout
    private int stepsPerKm;

    //calories = distance * stepsPerKm / stepsPerCalorie

    @Override
    public int calculateCaloriesBurned() {
        return getDistance() * this.stepsPerKm / this.stepsPerCalorie;
    }

    public void setStepsPerCalorie(int stepsPerCalorie) {
        this.stepsPerCalorie = stepsPerCalorie;
    }

    public void setStepsPerKm(int stepsPerKm) {
        this.stepsPerKm = stepsPerKm;
    }

    String getWorkoutSummary() {
        return null;
    }
}
