public abstract class Workout {

    private String location;
    private int duration;
    private int distance;

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    double calculateAvarageSpeed(){
        return 0;
    }

    String calculatePace(){
        return null;
    }

    abstract int calculateCaloriesBurned();

    abstract String getWorkoutSummary();


}
