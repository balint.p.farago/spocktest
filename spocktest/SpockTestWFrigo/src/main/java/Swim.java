public class Swim extends Workout{

    // defines how many calories were burned for every 100 stroke
    private int calPer100Stroke;

    //defines how many strokes the user did per pool length
    private int strokePerLength;

    //defines how long the pool is in meter
    int poolLength;

    // poolLength /   1. (distance / poolLength) * (calPer100Stroke * strokePerLength/100)


    public void setCalPer100Stroke(int calPer100Stroke) {
        this.calPer100Stroke = calPer100Stroke;
    }

    public void setStrokePerLength(int strokePerLength) {
        this.strokePerLength = strokePerLength;
    }

    public void setPoolLength(int poolLength) {
        this.poolLength = poolLength;
    }

    @Override
    int calculateCaloriesBurned() {
        double calorie = 0.0;
        calorie =  (getDistance() / (double)this.poolLength * (double)this.calPer100Stroke * (double)this.strokePerLength) / 100;
        return (int) calorie;
    }

    @Override
    String getWorkoutSummary() {
        return null;
    }
}
