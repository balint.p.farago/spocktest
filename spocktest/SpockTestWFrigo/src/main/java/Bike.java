import com.sun.corba.se.spi.orbutil.threadpool.Work;

public class Bike extends Workout {

    int avaragePower;

    @Override
    int calculateCaloriesBurned() {
        return this.avaragePower * getDuration() * 3600;
    }

    @Override
    String getWorkoutSummary() {
        return null;
    }

    public void setAvaragePower(int avaragePower) {
        this.avaragePower = avaragePower;
    }
}
